﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accelerate-Mission-Complete.aspx.cs" Inherits="Accelerate_Mission_Complete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Accelerate-Mission-Complete.aspx</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
     <asp:Literal ID="litStyleSheet" runat="server"></asp:Literal>
</head>
<body style="background-color:#fff;">
    <form id="form1" runat="server">
    <center>
        <div style="width:100%;">
        <div class="page_container">
            <div class="wrap">
                        <div class="container">
                            <div class="page_in">
                                <div class="container">
                                <section>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="completeMissionBody">
                                            <div class="completeMissionInner">
                                                <div class="completeMissionHighlight">
                                                    <br class="clr"/>
                                                    <br class="clr"/>
                                                    <img src="images/imgCongratsRibbon.png" title="" alt=""/>
                                                    <br class="clr"/>
                                                    <br class="clr"/>
                                                    <asp:Literal ID="litCurrentMission" runat="server"></asp:Literal>
               
                                                </div>
                                                <br class="clr"/>
                                                <br class="clr"/>
                                                 
                                            </div>
                                            <br class="clr"/>
                                            <br class="clr"/>
                                        </div>
                                    </div>
                                </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </center>
    </form>
</body>
</html>
