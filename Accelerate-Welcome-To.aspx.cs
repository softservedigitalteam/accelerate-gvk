﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Welcome_To : System.Web.UI.Page
{

    clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }

        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        clsGroups clsGroup = new clsGroups(clsAccountUsers.iGroupID);

        popMessage(clsGroup);
    }
    protected void btnSmallProceed_Click(object sender, EventArgs e)
    {
        Response.Redirect("Accelerate-Profile-Mission.aspx");
    }

    protected void btnProceedLarge_Click(object sender, EventArgs e)
    {
        Response.Redirect("Accelerate-Profile-Mission.aspx");
    }
    
    protected void popMessage(clsGroups clsGroup)
    {
        int iWelcomeMessageID = clsGroup.iWelcomeMessageID;

        clsWelcomeMessages clsWelcomeMessage = new clsWelcomeMessages(iWelcomeMessageID);
        string paragraph = clsWelcomeMessage.strParagraphText.Replace("\n", "<br /> ");

        StringBuilder strbTitle = new StringBuilder();
        StringBuilder strbMessage = new StringBuilder();
        StringBuilder strbImage = new StringBuilder();

        string strText = clsWelcomeMessage.strParagraphText;

        strbImage.AppendLine("<img id='imgFinal' src='/WelcomeMessages/" + clsWelcomeMessage.strPathToImages + "/" + clsWelcomeMessage.strMasterImage + "'/>");
        strbTitle.AppendLine("<p style='font-family: Arial; color: #fff; text-align: left; font-weight: 300;'>" + clsWelcomeMessage.strHeaderText + "</p>");
        strbMessage.AppendLine("<p style='font-family: Arial; color: #fff; text-align: left; font-weight: 300;'>");
        strbMessage.AppendLine(paragraph);
        strbMessage.AppendLine("</p>");

        litImage.Text = strbImage.ToString();
        litHeading.Text = strbTitle.ToString();
        litText.Text = strbMessage.ToString();
    }
}