﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" CodeFile="Accelerate-Current-Mission.aspx.cs" Inherits="Accelerate_Current_Mission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" EnableViewState="true">
        <ContentTemplate>
            <!-- iFrame to Prevent Session Timeouts -->
        <iframe id="Defib" src="KeepAlive.aspx" frameborder="0" width="0" height="0" runat="server"></iframe>
        <!-- iFrame to Prevent Session Timeouts -->

            <%--<div class="infobox">
                <div class="row-fluid">
                    <div class="span2 PointsSection">
                        <div style="width: 100%; font-size: 50px; font-weight: 300; text-align: center !important;">
                            <asp:Literal runat="server" ID="litPoints"></asp:Literal>
                        </div>
                        <br />
                        <br />
                        <div style="width: 100%; font-size: 20px; font-weight: bold; text-align: left; margin-left: 15px;">POINTS</div>
                    </div>
                    <div class="span10">
                        <div class="row-fluid">
                            <div class="showcaseSection Black">My showcase</div>
                            <asp:LinkButton runat="server" CssClass="phaseActive" OnClick="btnPhase1_Click" ID="btnPhase1">
                                <div class="showcaseSection Blue1">
                                    Phase 1:
                                    <asp:Literal runat="server" ID="litPhase1Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" CssClass="phaseActive" ID="btnPhase2" OnClick="btnPhase2_Click" Enabled="false">
                                <div class="showcaseSection Blue2">
                                    Phase 2:
                                    <asp:Literal runat="server" ID="litPhase2Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" CssClass="phaseActive" ID="btnPhase3" OnClick="btnPhase3_Click" Enabled="false">
                                <div class="showcaseSection Blue3">
                                    Phase 3:
                                    <asp:Literal runat="server" ID="litPhase3Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="well">
                            <asp:Repeater ID="rpAchievements" runat="server">
                                <ItemTemplate>
                                    <%#Eval ("strNewRow") %>
                                    <div class="achievement" style="text-align: center; margin: 0 1.2%;">
                                        <a href='<%#Eval ("strLink") %>'>
                                            <img onerror="this.src='images/imgAchievementEmpty.png'" src='<%#Eval ("FullPathForImage") %>' title='<%#Eval ("strTitle") %>' alt='<%#Eval ("strTitle") %>' width='70%' /></a>
                                        <br />
                                        <%#Eval ("strTitle") %>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>--%>
            <div class="infobox">
                <div class="top-bar">
                    <h3>Your Current Mission - Earn your badge here</h3>
                </div>
                <div class="well">
                    <asp:Literal ID="litCurrentMission" runat="server"></asp:Literal>
                    <div id="TopDiv" class="well-short" style="background: #80c342; padding-bottom: 12px;">
                        <div class="missionInformation" style="color: #000; font-size: 13px;">
                            <b><asp:Literal ID="litTitle" runat="server"></asp:Literal></b>
                        </div>
                        <br />
                        <asp:Literal ID="litBanner" runat="server"></asp:Literal>
                    </div>
                    <br />
                    <div class="well-short" style="background: #FFF; color: #000; padding-bottom: 12px;">
                        <b style="font-size: 13px;">Your Mission</b>
                        <br />
                        <br />
                        <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                        <asp:Literal ID="litLink" runat="server"></asp:Literal>
                        <asp:Literal ID="litVideo" runat="server"></asp:Literal>

                    </div>
                    <br />
                    <div class="well-short" style="background: #FFF; color: #000; padding-bottom: 12px;">
                        <b style="font-size: 13px;">Your Task</b>
                        <br />
                        <br />

                        <asp:UpdatePanel ID="upQs" runat="server" EnableViewState="true">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="phAnswers">
                                    <asp:Repeater runat="server" ID='rpMisionQuestions'>
                                        <ItemTemplate>
                                            <%#Eval ("strQuestion") %><br />
                                            <br />
                                            <asp:TextBox runat="server" ID="txtQuestionAnswer" CommandArgument='<%#Eval ("iMissionQuestionID") %>' CssClass="textbox"></asp:TextBox><br />
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="upQs2" runat="server" EnableViewState="true" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="phAnswers2">
                                    <asp:Repeater runat="server" ID='rpMissionQuestions2'>
                                        <ItemTemplate>
                                            <div style="float: left;">
                                                <img src='<%#Eval ("FullPathForImage") %>' width='70px' height='70px' />
                                            </div>
                                            <div style="float: left; padding-left: 20px; width: 75%">
                                                <asp:TextBox runat="server" ID="txtName" CommandArgument='<%#Eval ("iTeamShipID") %>' placeholder="Name" CssClass="textbox"></asp:TextBox>
                                                <asp:TextBox runat="server" ID="txtTitle" CommandArgument='<%#Eval ("iTeamShipID") %>' placeholder="Position" CssClass="textbox"></asp:TextBox>
                                            </div>
                                            <br style="clear: both" />
                                            <br style="clear: both" />
                                            <br style="clear: both" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="upQs3" runat="server" EnableViewState="True">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="PlaceHolder1">
                                    <asp:Repeater runat="server" ID='rpMissionDropDownQuestions'>
                                        <ItemTemplate><br style="clear: both" />
                                            <div class="span3">
                                                <%#Eval ("strQuestion") %>
                                            </div>
                                            <div class="span3" style="float: left; margin-top:-6px;">
                                                <asp:DropDownList ID="lstQuestionAnswer" runat="server" CommandArgument='<%#Eval ("iMissionDropDownQuestionID") %>' CssClass="textbox" Enabled="true"></asp:DropDownList><br />
                                            </div>
                                            <br style="clear: both" />
                                            <br style="clear: both" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Green3 btn" OnClick="btnSave_Click" EnableViewState="true" />
                    </div>
                </div>
            </div>
            

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; height: 100%; width: 100%; overflow: show; margin: auto; top: 0; left: 0; bottom: 0; right: 0; z-index: 9999999; background-color: #fff; opacity: 0.8;">
                        <center><asp:Image ID="imgUpdateProgress2" runat="server" ImageUrl="img/Loader.gif" AlternateText="Loading ..." ToolTip="Loading ..." style="opacity:1;position:fixed;top: 43%;left:50%"/></center>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" runat="Server">

    <script src='assets/js/jquery.hotkeys.js'></script>
    <script src='assets/js/calendar/fullcalendar.min.js'></script>
    <script src="assets/js/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="assets/js/jquery.pajinate.js"></script>
    <script src="assets/js/jquery.prism.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/charts/jquery.flot.time.js"></script>
    <script src="assets/js/charts/jquery.flot.pie.js"></script>
    <script src="assets/js/charts/jquery.flot.resize.js"></script>
    <asp:Literal ID="litBootstrap" runat="server"></asp:Literal>
    <script src="assets/js/bootstrap/bootstrap-wysiwyg.js"></script>
    <script src="assets/js/bootstrap/bootstrap-typeahead.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/jquery.chosen.min.js"></script>
    <script src="assets/js/avocado-custom.js"></script>
</asp:Content>

