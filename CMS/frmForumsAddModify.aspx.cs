using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmForumsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsForums clsForums;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popGroup();

            //### If the iForumID is passed through then we want to instantiate the object with that iForumID
            if ((Request.QueryString["iForumID"] != "") && (Request.QueryString["iForumID"] != null))
            {
                clsForums = new clsForums(Convert.ToInt32(Request.QueryString["iForumID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsForums = new clsForums();
            }
            Session["clsForums"] = clsForums;
        }
        else
        {
            clsForums = (clsForums)Session["clsForums"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmForumsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstGroup, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Forum added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Forum not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        lstGroup.SelectedValue = "0";
        clsValidation.SetValid(lstGroup);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsForums.strTitle.ToString();
         txtDescription.Text = clsForums.strDescription.ToString();
         lstGroup.SelectedValue = clsForums.iGroupID.ToString();
    }
    
    private void popGroup()
    {
         DataTable dtGroupsList = new DataTable();
         lstGroup.DataSource = clsGroups.GetGroupsList();

         //### Populates the drop down list with PK and TITLE;
         lstGroup.DataValueField = "iGroupID";
         lstGroup.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstGroup.DataBind();

         //### Add default select option;
         lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsForums.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsForums.iAddedBy = clsUsers.iUserID;
        clsForums.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsForums.iEditedBy = clsUsers.iUserID;
        clsForums.strTitle = txtTitle.Text;
        clsForums.strDescription = txtDescription.Text;
        clsForums.iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());

        clsForums.Update();

        Session["dtForumsList"] = null;

        //### Go back to view page
        Response.Redirect("frmForumsView.aspx");
    }

    #endregion
}