
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsMissionStatussView
/// </summary>
public partial class CMS_clsMissionStatussView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionStatussList;

    List<clsMissionStatuss> glstMissionStatuss;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionStatuss"] == null)
            {
                glstMissionStatuss = new List<clsMissionStatuss>();
                Session["glstMissionStatuss"] = glstMissionStatuss;
            }
            else
                glstMissionStatuss = (List<clsMissionStatuss>)Session["glstMissionStatuss"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtMissionStatussList = clsMissionStatuss.GetMissionStatussList(FilterExpression, "");

        Session["dtMissionStatussList"] = dtMissionStatussList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionStatusesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionStatussList object
        try
        {
            dtMissionStatussList = new DataTable();

            if (Session["dtMissionStatussList"] == null)
                dtMissionStatussList = clsMissionStatuss.GetMissionStatussList();
            else
                dtMissionStatussList = (DataTable)Session["dtMissionStatussList"];

            dgrGrid.DataSource = dtMissionStatussList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionStatussView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionStatussView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionStatussView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionStatusID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionStatuss.Delete(iMissionStatusID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionStatussView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionStatussList = new DataTable();

        if (Session["dtMissionStatussList"] == null)
            dtMissionStatussList = clsMissionStatuss.GetMissionStatussList();
        else
            dtMissionStatussList = (DataTable)Session["dtMissionStatussList"];

        DataView dvTemp = dtMissionStatussList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionStatussList = dvTemp.ToTable();
        Session["dtMissionStatussList"] = dtMissionStatussList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionStatusID
            int iMissionStatusID = 0;
            iMissionStatusID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionStatussAddModify.aspx?action=edit&iMissionStatusID=" + iMissionStatusID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionStatusID
             int iMissionStatusID = 0;
             iMissionStatusID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionStatusesView.aspx?action=delete&iMissionStatusID=" + iMissionStatusID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region MissionStatuss FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtMissionStatuss = clsMissionStatuss.GetMissionStatussList(FilterExpression, "");
        List<string> glstMissionStatuss = new List<string>();

        if (dtMissionStatuss.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionStatuss in dtMissionStatuss.Rows)
            {
                glstMissionStatuss.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionStatuss["strTitle"].ToString(), dtrMissionStatuss["iMissionStatusID"].ToString()));
            }
        }
        else
            glstMissionStatuss.Add("No MissionStatuss Available.");
        strReturnList = glstMissionStatuss.ToArray();
        return strReturnList;
    }

    #endregion
}
