using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmDocumentsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsDocuments clsDocuments;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../frmCMSLogin.aspx");
            }
        }

        clsUsers = (clsUsers)Session["clsUsers"];

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveDocument"))
            DeleteDocument(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
            //### If the iDocumentID is passed through then we want to instantiate the object with that iDocumentID
            if ((Request.QueryString["iDocumentID"] != "") && (Request.QueryString["iDocumentID"] != null))
            {
                clsDocuments = new clsDocuments(Convert.ToInt32(Request.QueryString["iDocumentID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsDocuments = new clsDocuments();
            }
            Session["clsDocuments"] = clsDocuments;
        }
        else
        {
            clsDocuments = (clsDocuments)Session["clsDocuments"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmDocumentsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        if (lblUniquePath.Text == "")
        {
            bCanSave = false;
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please upload a document and fill out all mandatory field - TeamShip not addeds</div></div>";

        }
        else
        {


            if (bCanSave == true)
            {
                mandatoryDiv.Visible = false;
                lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Document added successfully</div></div>";
                SaveData();
            }
            else
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Document not added</div></div>";
            }
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);

        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtTitle.Text = clsDocuments.strTitle;
        txtDescription.Text = clsDocuments.strDescription;

        //### Populates images
        if (!string.IsNullOrEmpty(clsDocuments.strPathToDocument))
        {
            lblUniquePath.Text = clsDocuments.strPathToDocument;
            lblFileSize.Text = clsDocuments.strFileSize;
            getList(clsDocuments.strPathToDocument);
        }

        //clsCommonFunctions.setValidationImageValid(this);
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsDocuments.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsDocuments.iAddedBy = clsUsers.iUserID;
        clsDocuments.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsDocuments.iEditedBy = clsUsers.iUserID;

        clsDocuments.strTitle = txtTitle.Text;
        clsDocuments.strDescription = txtDescription.Text;

        //### Images related items
        clsDocuments.strPathToDocument = lblUniquePath.Text;
        clsDocuments.strFileSize = lblFileSize.Text;

        clsDocuments.Update();

        Session["dtDocumentsList"] = null;

        //### Go back to view page
        Response.Redirect("frmDocumentsView.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstDocument;
    List<string> lstDocumentFileNames;
    int iMaxDocument = 1;

    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Documents";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstDocument"] == null)
        {
            lstDocument = new List<string>();
            Session["lstDocument"] = lstDocument;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Document in the datalist
        if (dlDocument.Items.Count == iMaxDocument)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">You can only upload " + iMaxDocument.ToString() + " document per entry.</div></div>";
            getList(lblUniquePath.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + FileUpload.ClientID + "',true)", true);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }

            UploadDocument(strUniquePath);
            getList(strUniquePath);
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\Documents" + iCount) == true)
        {
            iCount++;
        }
        return "Documents" + iCount;
    }

    protected void UploadDocument(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 5242880)
        {
            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);

            string strSaveLocation = "";
            strSaveLocation = strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);

            if (File.Exists(strSaveLocation))
                File.Delete(strSaveLocation);

            FileUpload.PostedFile.SaveAs(strUniqueFullPath + "\\" + strSaveLocation);

            lblFileSize.Text = FileUpload.PostedFile.ContentLength.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + FileUpload.ClientID + "',true)", true);
        }
        else
        {
            lblUploadError.Text = "The file should be between 0 and 3MB.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + FileUpload.ClientID + "',false)", true);
        }

        lblUploadError.Visible = true;
    }

    public void getList(String strPathToFolder)
    {
        lstDocument = new List<string>();
        lstDocumentFileNames = new List<string>();

        try
        {
            String[] straFileList = Directory.GetFiles(strUniqueFullPath + "\\" + strPathToFolder);
            foreach (String strFile in straFileList)
            {
                int iImageCount = 0;
                if (strFile.Contains(".pdf"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"], System.Configuration.ConfigurationManager.AppSettings["WebRoot"]).Replace("\\", "/");

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center>
                                            <a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_pdf.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".doc") || strFile.Contains(".docx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"], System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_word.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".xls") || strFile.Contains(".xlsx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"], System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_excel.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".ppt") || strFile.Contains(".pptx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"], System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_powerpoint.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
            }

            dlDocument.DataSource = lstDocument;
            dlDocument.DataBind();

            Session["lstDocument"] = lstDocument;
            Session["lstDocumentFileNames"] = lstDocumentFileNames;
        }
        catch (Exception ex)
        { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstDocumentFileNames = (List<string>)Session["lstDocumentFileNames"];
                strReturn = lstDocumentFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteDocument(int iDocumentIndex)
    {

        //### Deletes all Document related to the target Document.
        lstDocument = (List<string>)Session["lstDocument"];
        lstDocumentFileNames = (List<string>)Session["lstDocumentFileNames"];

        lstDocument.RemoveAt(iDocumentIndex);
        Session["lstDocument"] = lstDocument;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);

        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstDocumentFileNames[iDocumentIndex].ToString())
            {
                //### Remove all Document
                File.Delete(file);
                break;
            }
        }

        lstDocumentFileNames.RemoveAt(iDocumentIndex);
        ViewState["lstDocumentFileNames"] = lstDocumentFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}