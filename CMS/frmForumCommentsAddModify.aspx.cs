using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmForumCommentsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsForumComments clsForumComments;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            //### If the iForumCommentID is passed through then we want to instantiate the object with that iForumCommentID
            if ((Request.QueryString["iForumCommentID"] != "") && (Request.QueryString["iForumCommentID"] != null))
            {
                clsForumComments = new clsForumComments(Convert.ToInt32(Request.QueryString["iForumCommentID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsForumComments = new clsForumComments();
            }
            Session["clsForumComments"] = clsForumComments;
        }
        else
        {
            clsForumComments = (clsForumComments)Session["clsForumComments"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmForumCommentsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       //bCanSave = clsValidation.IsNullOrEmpty(lstAccountUser, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtComment, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">ForumComment added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - ForumComment not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstAccountUser.SelectedValue = "0";
        clsValidation.SetValid(lstAccountUser);
        txtComment.Text = "";
        clsValidation.SetValid(txtComment);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtComment.Text = clsForumComments.strComment;
    }
    
    //private void popAccountUser()
    //{
    //     DataTable dtAccountUsersList = new DataTable();
    //     lstAccountUser.DataSource = clsAccountUsers.GetAccountUsersList();

    //     //### Populates the drop down list with PK and TITLE;
    //     lstAccountUser.DataValueField = "iAccountUserID";
    //     lstAccountUser.DataTextField = "strTitle";

    //     //### Bind the data to the list;
    //     lstAccountUser.DataBind();

    //     //### Add default select option;
    //     lstAccountUser.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsForumComments.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsForumComments.iAddedBy = clsUsers.iUserID;
        clsForumComments.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsForumComments.iEditedBy = clsUsers.iUserID;
        //clsForumComments.iForumID = 0;
        clsForumComments.strComment = txtComment.Text;

        clsForumComments.Update();

        Session["dtForumCommentsList"] = null;

        //### Go back to view page
        Response.Redirect("frmForumCommentsView.aspx");
    }

    #endregion
}