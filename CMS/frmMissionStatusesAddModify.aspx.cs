using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionStatussAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissionStatuss clsMissionStatuss;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iMissionStatusID is passed through then we want to instantiate the object with that iMissionStatusID
            if ((Request.QueryString["iMissionStatusID"] != "") && (Request.QueryString["iMissionStatusID"] != null))
            {
                clsMissionStatuss = new clsMissionStatuss(Convert.ToInt32(Request.QueryString["iMissionStatusID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMissionStatuss = new clsMissionStatuss();
            }
            Session["clsMissionStatuss"] = clsMissionStatuss;
        }
        else
        {
            clsMissionStatuss = (clsMissionStatuss)Session["clsMissionStatuss"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionStatusesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">MissionStatus added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - MissionStatus not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsMissionStatuss.strTitle;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissionStatuss.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionStatuss.iAddedBy = clsUsers.iUserID;
        clsMissionStatuss.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionStatuss.iEditedBy = clsUsers.iUserID;
        clsMissionStatuss.strTitle = txtTitle.Text;

        clsMissionStatuss.Update();

        Session["dtMissionStatussList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionStatusesView.aspx");
    }

    #endregion
}