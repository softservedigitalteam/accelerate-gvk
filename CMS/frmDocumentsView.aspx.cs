
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsDocumentsView
/// </summary>
public partial class CMS_clsDocumentsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtDocumentsList;

    List<clsDocuments> glstDocuments;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../frmCMSLogin.aspx");
            }
        }

        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstDocuments"] == null)
            {
                glstDocuments = new List<clsDocuments>();
                Session["glstDocuments"] = glstDocuments;
            }
            else
                glstDocuments = (List<clsDocuments>)Session["glstDocuments"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle + ' ' + strDescription) LIKE '%" + hfDocumentID.Value + "%'"; 
        DataTable dtDocumentsList = clsDocuments.GetDocumentsList(FilterExpression, "");

        Session["dtDocumentsList"] = dtDocumentsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmDocumentsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsDocumentsList object
        try
        {
            dtDocumentsList = new DataTable();

            if (Session["dtDocumentsList"] == null)
                dtDocumentsList = clsDocuments.GetDocumentsList();
            else
                dtDocumentsList = (DataTable)Session["dtDocumentsList"];

            dgrGrid.DataSource = dtDocumentsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["DocumentsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["DocumentsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["DocumentsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iDocumentID = int.Parse((sender as LinkButton).CommandArgument);

        clsDocuments.Delete(iDocumentID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["DocumentsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtDocumentsList = new DataTable();

        if (Session["dtDocumentsList"] == null)
            dtDocumentsList = clsDocuments.GetDocumentsList();
        else
            dtDocumentsList = (DataTable)Session["dtDocumentsList"];

        DataView dvTemp = dtDocumentsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtDocumentsList = dvTemp.ToTable();
        Session["dtDocumentsList"] = dtDocumentsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iDocumentID
            int iDocumentID = 0;
            iDocumentID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmDocumentsAddModify.aspx?action=edit&iDocumentID=" + iDocumentID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iDocumentID
            int iDocumentID = 0;
            iDocumentID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmDocumentsView.aspx?action=delete&iDocumentID=" + iDocumentID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Documents FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "strTitle LIKE '%" + prefixText + "%' OR strDescription LIKE '%" + prefixText + "%'";
        DataTable dtDocuments = clsDocuments.GetDocumentsList(FilterExpression, "");
        List<string> glstDocuments = new List<string>();

        if (dtDocuments.Rows.Count > 0)
        {
            foreach (DataRow dtrDocuments in dtDocuments.Rows)
            {
                glstDocuments.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dtrDocuments["strTitle"].ToString() + " " + dtrDocuments["strDescription"].ToString(), 
                    dtrDocuments["strTitle"].ToString() + " " + dtrDocuments["strDescription"].ToString()));
            }
        }
        else
            glstDocuments.Add("No Documents Available.");

        strReturnList = glstDocuments.ToArray();
        return strReturnList;
    }

    #endregion
}
