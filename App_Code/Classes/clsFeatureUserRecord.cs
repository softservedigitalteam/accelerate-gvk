
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsFeatureUsers
/// </summary>
public class clsFeatureUsers
{
    #region MEMBER VARIABLES

        private int m_iFeatureUserID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iAccountUserID;
        private int m_iFeatureID;
        private String m_strFeatureChecklist;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iFeatureUserID
        {
            get
            {
                return m_iFeatureUserID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iAccountUserID
        {
            get
            {
                return m_iAccountUserID;
            }
            set
            {
                m_iAccountUserID = value;
            }
        }
        public int iFeatureID
        {
            get
            {
                return m_iFeatureID;
            }
            set
            {
                m_iFeatureID = value;
            }
        }

        public String strFeatureChecklist
        {
            get
            {
                return m_strFeatureChecklist;
            }
            set
            {
                m_strFeatureChecklist = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsFeatureUsers()
    {
        m_iFeatureUserID = 0;
    }

    public clsFeatureUsers(int iFeatureUserID)
    {
        m_iFeatureUserID = iFeatureUserID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iFeatureUserID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iFeatureID", m_iFeatureID),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
                        new SqlParameter("@strFeatureChecklist", m_strFeatureChecklist)
                  };

                  //### Add
                    m_iFeatureUserID = (int)clsDataAccess.ExecuteScalar("spFeatureUsersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iFeatureUserID", m_iFeatureUserID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
                         new SqlParameter("@iFeatureID", m_iFeatureID),
                         new SqlParameter("@strFeatureChecklist", m_strFeatureChecklist)
                    };
                    //### Update
                    clsDataAccess.Execute("spFeatureUsersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iFeatureUserID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iFeatureUserID", iFeatureUserID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spFeatureUsersDelete", sqlParameter);
        }

        public static DataTable GetPortalFeaturesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spFeatureUsersList", EmptySqlParameter);
        }

        public static DataTable GetPortalFeaturesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvPortalFeaturesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvPortalFeaturesList = clsDataAccess.GetDataView("spFeatureUsersList", EmptySqlParameter);
            dvPortalFeaturesList.RowFilter = strFilterExpression;
            dvPortalFeaturesList.Sort = strSortExpression;

            return dvPortalFeaturesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iFeatureUserID", m_iFeatureUserID)
                        };
                    DataRow drRecord = clsDataAccess.GetRecord("spFeatureUsersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
                m_iFeatureID = Convert.ToInt32(drRecord["iFeatureID"]);
                m_strFeatureChecklist = drRecord["strFeatureChecklist"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}