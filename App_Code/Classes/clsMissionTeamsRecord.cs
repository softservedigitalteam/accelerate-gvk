
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionTeams
/// </summary>
public class clsMissionTeams
{
    #region MEMBER VARIABLES

        private int m_iMissionTeamID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionQuestionID;
        private String m_strName;
        private String m_strLeadershipTitle;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iMissionTeamID
        {
            get
            {
                return m_iMissionTeamID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionQuestionID
        {
            get
            {
                return m_iMissionQuestionID;
            }
            set
            {
                m_iMissionQuestionID = value;
            }
        }

        public String strName
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        public String strLeadershipTitle
        {
            get
            {
                return m_strLeadershipTitle;
            }
            set
            {
                m_strLeadershipTitle = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsMissionTeams()
    {
        m_iMissionTeamID = 0;
    }

    public clsMissionTeams(int iMissionTeamID)
    {
        m_iMissionTeamID = iMissionTeamID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iMissionTeamID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionQuestionID", m_iMissionQuestionID),
                        new SqlParameter("@strName", m_strName),
                        new SqlParameter("@strLeadershipTitle", m_strLeadershipTitle),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage)                  
                  };

                  //### Add
                  m_iMissionTeamID = (int)clsDataAccess.ExecuteScalar("spMissionTeamsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionTeamID", m_iMissionTeamID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionQuestionID", m_iMissionQuestionID),
                         new SqlParameter("@strName", m_strName),
                         new SqlParameter("@strLeadershipTitle", m_strLeadershipTitle),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage)
                    };
                    //### Update
                    clsDataAccess.Execute("spMissionTeamsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iMissionTeamID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionTeamID", iMissionTeamID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionTeamsDelete", sqlParameter);
        }

        public static DataTable GetMissionTeamsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spMissionTeamsList", EmptySqlParameter);
        }
        public static DataTable GetMissionTeamsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvMissionTeamsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvMissionTeamsList = clsDataAccess.GetDataView("spMissionTeamsList", EmptySqlParameter);
            dvMissionTeamsList.RowFilter = strFilterExpression;
            dvMissionTeamsList.Sort = strSortExpression;

            return dvMissionTeamsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionTeamID", m_iMissionTeamID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spMissionTeamsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iMissionQuestionID = Convert.ToInt32(drRecord["iMissionQuestionID"]);
                m_strName = drRecord["strName"].ToString();
                m_strLeadershipTitle = drRecord["strLeadershipTitle"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}