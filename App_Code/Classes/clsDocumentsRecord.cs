using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsDocuments
/// </summary>
public class clsDocuments
{
    #region MEMBER VARIABLES

        private int m_iDocumentID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strFileSize;
        private String m_strTitle;
        private String m_strDescription;
        private String m_strPathToDocument;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iDocumentID
        {
            get
            {
                return m_iDocumentID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strFileSize
        {
            get
            {
                return m_strFileSize;
            }
            set
            {
                m_strFileSize = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strDescription
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        public String strPathToDocument
        {
            get
            {
                return m_strPathToDocument;
            }
            set
            {
                m_strPathToDocument = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsDocuments()
    {
        m_iDocumentID = 0;
    }

    public clsDocuments(int iDocumentID)
    {
        m_iDocumentID = iDocumentID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iDocumentID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strFileSize", m_strFileSize),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strPathToDocument", m_strPathToDocument)                  
                  };

                  //### Add
                  m_iDocumentID = (int)clsDataAccess.ExecuteScalar("spDocumentsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iDocumentID", m_iDocumentID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strFileSize", m_strFileSize),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strPathToDocument", m_strPathToDocument)
                    };
                    //### Update
                    clsDataAccess.Execute("spDocumentsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iDocumentID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iDocumentID", iDocumentID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spDocumentsDelete", sqlParameter);
        }

        public static DataTable GetDocumentsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spDocumentsList", EmptySqlParameter);
        }
        public static DataTable GetDocumentsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvDocumentsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvDocumentsList = clsDataAccess.GetDataView("spDocumentsList", EmptySqlParameter);
            dvDocumentsList.RowFilter = strFilterExpression;
            dvDocumentsList.Sort = strSortExpression;

            return dvDocumentsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iDocumentID", m_iDocumentID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spDocumentsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strFileSize = drRecord["strFileSize"].ToString();
                m_strTitle = drRecord["strTitle"].ToString();
                m_strDescription = drRecord["strDescription"].ToString();
                m_strPathToDocument = drRecord["strPathToDocument"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}