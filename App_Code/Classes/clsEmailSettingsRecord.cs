using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEmailSettings
/// </summary>
public class clsEmailSettings
{
    #region MEMBER VARIABLES

        private int m_iEmailSettingID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iAccountUserID;
        private bool m_bEmailMissions;
        private bool m_bEmailForumMessages;
        private bool m_bEmailPhaseReminders;
        private bool m_bEmailReminders;
        private bool m_bEmailNotification;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iEmailSettingID
        {
            get
            {
                return m_iEmailSettingID;
            }
            set
            {
                m_iEmailSettingID = value;
            }
        }

        public int iAccountUserID
        {
            get
            {
                return m_iAccountUserID;
            }
            set
            {
                m_iAccountUserID = value;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public Boolean bEmailMissions
        {
            get
            {
                return m_bEmailMissions;
            }
            set
            {
                m_bEmailMissions = value;
            }
        }

        public Boolean bEmailReminders
        {
            get
            {
                return m_bEmailReminders;
            }
            set
            {
                m_bEmailReminders = value;
            }
        }


        public Boolean bEmailNotification
        {
            get
            {
                return m_bEmailNotification;
            }
            set
            {
                m_bEmailNotification = value;
            }
        }


        public Boolean bEmailForumMessages
        {
            get
            {
                return m_bEmailForumMessages;
            }
            set
            {
                m_bEmailForumMessages = value;
            }
        }

        public Boolean bEmailPhaseReminders
        {
            get
            {
                return m_bEmailPhaseReminders;
            }
            set
            {
                m_bEmailPhaseReminders = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsEmailSettings()
    {
        m_iEmailSettingID = 0;
    }

    public clsEmailSettings(int iEmailSettingID)
    {
        m_iEmailSettingID = iEmailSettingID;
        GetData();
    }

    public clsEmailSettings(bool bGetByUser, int iAccountUserID)
    {
        m_iAccountUserID = iAccountUserID;
        GetDataByUserID();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iEmailSettingID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
                        new SqlParameter("@bEmailMissions", m_bEmailMissions),
                        new SqlParameter("@bEmailNotification", m_bEmailNotification),
                        new SqlParameter("@bEmailReminders", m_bEmailReminders),
                    };

                    //### Add
                    m_iEmailSettingID = (int)clsDataAccess.ExecuteScalar("spEmailSettingsInsert", sqlParametersInsert);
     
                }
                else
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                            new SqlParameter("@iEmailSettingID", m_iEmailSettingID),
                            new SqlParameter("@dtEdited", m_dtEdited),
                            new SqlParameter("@iEditedBy", m_iEditedBy),
                            new SqlParameter("@iAccountUserID", m_iAccountUserID),
                            new SqlParameter("@bEmailMission", m_bEmailMissions),
                            new SqlParameter("@bEmailNotification", m_bEmailNotification),
                            new SqlParameter("@bEmailReminders", m_bEmailReminders),
                    };

                    //### Update
                    clsDataAccess.Execute("spEmailSettingsUpdate", sqlParametersUpdate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iEmailSettingID)
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@iEmailSettingID", iEmailSettingID)
            };

            //### Executes delete sp
            clsDataAccess.Execute("spEmailSettingsDelete", sqlParameter);
        }

        public static DataTable GetSettingsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spEmailSettingsList", EmptySqlParameter);
        }

        public static DataTable GetSettingsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvRanksList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvRanksList = clsDataAccess.GetDataView("spEmailSettingsList", EmptySqlParameter);
            dvRanksList.RowFilter = strFilterExpression;
            dvRanksList.Sort = strSortExpression;

            return dvRanksList.ToTable();
        }

    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
            {
                //### Populate
                SqlParameter[] sqlParameter = new SqlParameter[] 
                {
                    new SqlParameter("@iEmailsettingID", m_iEmailSettingID)
                };
                DataRow drRecord = clsDataAccess.GetRecord("spEmailSettingsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                    m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

                if (drRecord["iEditedBy"] != DBNull.Value)
                    m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iEmailSettingID = Convert.ToInt32(drRecord["iEmailsettingID"]);

                m_bEmailMissions = Convert.ToBoolean(drRecord["bEmailMissions"]);
                //m_bEmailForumMessages = Convert.ToBoolean(drRecord["bEmailForumMessages"]);
                //m_bEmailPhaseReminders = Convert.ToBoolean(drRecord["bEmailPhaseReminders"]);
                m_bEmailReminders = Convert.ToBoolean(drRecord["bEmailReminders"]);
                m_bEmailNotification = Convert.ToBoolean(drRecord["bEmailNotification"]);

                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]); 
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected virtual void GetDataByUserID()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                    {
                        new SqlParameter("@iAccountUserID", m_iAccountUserID)
                    };
                    DataRow drRecord = clsDataAccess.GetRecord("spEmailSettingsGetRecordByUserID", sqlParameter);

                    m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                    m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                    if (drRecord["dtEdited"] != DBNull.Value)
                        m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

                    if (drRecord["iEditedBy"] != DBNull.Value)
                        m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                    m_iEmailSettingID = Convert.ToInt32(drRecord["iEmailsettingID"]);

                    m_bEmailMissions = Convert.ToBoolean(drRecord["bEmailMissions"]);
                    //m_bEmailForumMessages = Convert.ToBoolean(drRecord["bEmailForumMessages"]);
                    m_bEmailReminders = Convert.ToBoolean(drRecord["bEmailReminders"]);
                    m_bEmailNotification = Convert.ToBoolean(drRecord["bEmailNotification"]);

                    m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    #endregion
}