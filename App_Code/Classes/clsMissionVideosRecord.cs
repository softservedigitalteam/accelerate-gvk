
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionVideos
/// </summary>
public class clsMissionVideos
{
    #region MEMBER VARIABLES

    private int m_iMissionVideoID;
    private int m_iAddedBy;
    private DateTime m_dtAdded;
    private int m_iEditedBy;
    private DateTime m_dtEdited;
    private String m_strTitle;
    private String m_strColor;
    private String m_strVideoLink;
    private bool m_bIsFullsize;
    private String m_strPathToVideo;
    private String m_strMasterVideo;
    private String m_strPathToImages;
    private String m_strMasterImage;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iMissionVideoID
    {
        get
        {
            return m_iMissionVideoID;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strColor
    {
        get
        {
            return m_strColor;
        }
        set
        {
            m_strColor = value;
        }
    }

    public String strVideoLink
    {
        get
        {
            return m_strVideoLink;
        }
        set
        {
            m_strVideoLink = value;
        }
    }

    public bool bIsFullsize
    {
        get
        {
            return m_bIsFullsize;
        }
        set
        {
            m_bIsFullsize = value;
        }
    }

    public String strPathToVideo
    {
        get
        {
            return m_strPathToVideo;
        }
        set
        {
            m_strPathToVideo = value;
        }
    }

    public String strMasterVideo
    {
        get
        {
            return m_strMasterVideo;
        }
        set
        {
            m_strMasterVideo = value;
        }
    }

    public String strPathToImages
    {
        get
        {
            return m_strPathToImages;
        }
        set
        {
            m_strPathToImages = value;
        }
    }

    public String strMasterImage
    {
        get
        {
            return m_strMasterImage;
        }
        set
        {
            m_strMasterImage = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsMissionVideos()
    {
        m_iMissionVideoID = 0;
    }

    public clsMissionVideos(int iMissionVideoID)
    {
        m_iMissionVideoID = iMissionVideoID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iMissionVideoID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strColor", m_strColor),
                        new SqlParameter("@strVideoLink", m_strVideoLink),
                        new SqlParameter("@bIsFullsize", m_bIsFullsize),
                        new SqlParameter("@strPathToVideo", m_strPathToVideo),
                        new SqlParameter("@strMasterVideo", m_strMasterVideo),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage) 
                  };

                //### Add
                m_iMissionVideoID = (int)clsDataAccess.ExecuteScalar("spMissionVideosInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionVideoID", m_iMissionVideoID),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strColor", m_strColor),
                         new SqlParameter("@strVideoLink", m_strVideoLink),
                         new SqlParameter("@bIsFullsize", m_bIsFullsize),
                         new SqlParameter("@strPathToVideo", m_strPathToVideo),
                         new SqlParameter("@strMasterVideo", m_strMasterVideo),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage) 
                    };
                //### Update
                clsDataAccess.Execute("spMissionVideosUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iMissionVideoID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionVideoID", iMissionVideoID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionVideosDelete", sqlParameter);
    }

    public static DataTable GetMissionVideosList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spMissionVideosList", EmptySqlParameter);
    }
    public static DataTable GetMissionVideosList(string strFilterExpression, string strSortExpression)
    {
        DataView dvMissionVideosList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvMissionVideosList = clsDataAccess.GetDataView("spMissionVideosList", EmptySqlParameter);
        dvMissionVideosList.RowFilter = strFilterExpression;
        dvMissionVideosList.Sort = strSortExpression;

        return dvMissionVideosList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionVideoID", m_iMissionVideoID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spMissionVideosGetRecord", sqlParameter);

            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);
            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);


            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);
            m_strTitle = drRecord["strTitle"].ToString();
            m_strColor = drRecord["strColor"].ToString();
            m_strVideoLink = drRecord["strVideoLink"].ToString();
            m_bIsFullsize = Convert.ToBoolean(drRecord["bIsFullsize"]);
            m_strPathToVideo = drRecord["strPathToVideo"].ToString();
            m_strMasterVideo = drRecord["strMasterVideo"].ToString();
            m_strPathToImages = drRecord["strPathToImages"].ToString();
            m_strMasterImage = drRecord["strMasterImage"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}