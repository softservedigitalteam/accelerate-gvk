using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsGroupFeatures
/// </summary>
public class clsGroupFeatures
{
    #region MEMBER VARIABLES

        private int m_iPortalFeatureGroupsID;
        private String m_strTitle;
        private int m_iGroupID;
        private int m_iPortalFeatureID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iPortalFeatureGroupsID
        {
            get
            {
                return m_iPortalFeatureGroupsID;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public int iGroupID
        {
            get
            {
                return m_iGroupID;
            }
            set
            {
                m_iGroupID = value;
            }
        }

        public int iPortalFeatureID
        {
            get
            {
                return m_iPortalFeatureID;
            }
            set
            {
                m_iPortalFeatureID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsGroupFeatures()
    {
        m_iPortalFeatureGroupsID = 0;
    }

    public clsGroupFeatures(int iPortalFeatureGroupsID)
    {
        m_iPortalFeatureGroupsID = iPortalFeatureGroupsID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iPortalFeatureGroupsID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@iGroupID", m_iGroupID),
                        new SqlParameter("@iPortalFeatureID", m_iPortalFeatureID)                  
                  };

                  //### Add
                  m_iPortalFeatureGroupsID = (int)clsDataAccess.ExecuteScalar("spPortalFeatureGroupsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iPortalFeatureGroupsID", m_iPortalFeatureGroupsID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@iPortalFeatureID", m_iPortalFeatureID)
                    };
                    //### Update
                    clsDataAccess.Execute("spPortalFeatureGroupsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iPortalFeatureGroupsID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iPortalFeatureGroupsID", iPortalFeatureGroupsID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spPortalFeatureGroupsDelete", sqlParameter);
        }

        public static DataTable GetGroupFeaturesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spPortalFeatureGroupsList", EmptySqlParameter);
        }
        public static DataTable GetGroupFeaturesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvGroupFeaturesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvGroupFeaturesList = clsDataAccess.GetDataView("spPortalFeatureGroupsList", EmptySqlParameter);
            dvGroupFeaturesList.RowFilter = strFilterExpression;
            dvGroupFeaturesList.Sort = strSortExpression;

            return dvGroupFeaturesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iPortalFeatureGroupsID", m_iPortalFeatureGroupsID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spPortalFeatureGroupsGetRecord", sqlParameter);

                m_strTitle = drRecord["strTitle"].ToString();
                m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
                m_iPortalFeatureID = Convert.ToInt32(drRecord["iPortalFeatureID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}