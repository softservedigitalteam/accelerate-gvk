
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCustomGroupRanks
/// </summary>
public class clsCustomGroupRanks
{
    #region MEMBER VARIABLES

        private int m_iCustomGroupRankID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iGroupID;
        private String m_strTag;
        private String m_strRank1Title;
        private int m_iRank1Points;
        private String m_strRank2Title;
        private int m_iRank2Points;
        private String m_strRank3Title;
        private int m_iRank3Points;
        private String m_strRank4Title;
        private int m_iRank4Points;
        private String m_strRank5Title;
        private int m_iRank5Points;
        private String m_strRank6Title;
        private int m_iRank6Points;
        private String m_strRank7Title;
        private int m_iRank7Points;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iCustomGroupRankID
        {
            get
            {
                return m_iCustomGroupRankID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }



        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iGroupID
        {
            get
            {
                return m_iGroupID;
            }
            set
            {
                m_iGroupID = value;
            }
        }

        public String strTag
        {
            get
            {
                return m_strTag;
            }
            set
            {
                m_strTag = value;
            }
        }

        public String strRank1Title
        {
            get
             {
                return m_strRank1Title;
            }
            set
            {
                m_strRank1Title = value;
           }
        }

        public int iRank1Points
        {
            get
            {
                return m_iRank1Points;
            }
            set
            {
                m_iRank1Points = value;
            }
        }

        public String strRank2Title
        {
            get
            {
                return m_strRank2Title;
            }
            set
            {
                m_strRank2Title = value;
            }
        }

        public int iRank2Points
        {
            get
            {
                return m_iRank2Points;
            }
            set
            {
                m_iRank2Points = value;
            }
        }

        public String strRank3Title
        {
            get
            {
                return m_strRank3Title;
            }
            set
            {
                m_strRank3Title = value;
            }
        }

        public int iRank3Points
        {
            get
            {
                return m_iRank3Points;
            }
            set
            {
                m_iRank3Points = value;
            }
        }

        public String strRank4Title
        {
            get
            {
                return m_strRank4Title;
            }
            set
            {
                m_strRank4Title = value;
            }
        }

        public int iRank4Points
        {
            get
            {
                return m_iRank4Points;
            }
            set
            {
                m_iRank4Points = value;
            }
        }

        public String strRank5Title
        {
            get
            {
                return m_strRank5Title;
            }
            set
            {
                m_strRank5Title = value;
            }
        }

        public int iRank5Points
        {
            get
            {
                return m_iRank5Points;
            }
            set
            {
                m_iRank5Points = value;
            }
        }

        public String strRank6Title
        {
            get
            {
                return m_strRank6Title;
            }
            set
            {
                m_strRank6Title = value;
            }
        }

        public int iRank6Points
        {
            get
            {
                return m_iRank6Points;
            }
            set
            {
                m_iRank6Points = value;
            }
        }

        public String strRank7Title
        {
            get
            {
                return m_strRank7Title;
            }
            set
            {
                m_strRank7Title = value;
            }
        }

        public int iRank7Points
        {
            get
            {
                return m_iRank7Points;
            }
            set
            {
                m_iRank7Points = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCustomGroupRanks()
    {
        m_iCustomGroupRankID = 0;
    }

    public clsCustomGroupRanks(int iCustomGroupRankID)
    {
        m_iCustomGroupRankID = iCustomGroupRankID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCustomGroupRankID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iGroupID", m_iGroupID),
                        new SqlParameter("@strTag", m_strTag),
                        new SqlParameter("@strRank1Title", m_strRank1Title),
                        new SqlParameter("@iRank1Points", m_iRank1Points),         
                        new SqlParameter("@strRank2Title", m_strRank2Title),
                        new SqlParameter("@iRank2Points", m_iRank2Points),
                        new SqlParameter("@strRank3Title", m_strRank3Title),
                        new SqlParameter("@iRank3Points", m_iRank3Points),
                        new SqlParameter("@strRank4Title", m_strRank4Title),
                        new SqlParameter("@iRank4Points", m_iRank4Points),
                        new SqlParameter("@strRank5Title", m_strRank5Title),
                        new SqlParameter("@iRank5Points", m_iRank5Points),
                        new SqlParameter("@strRank6Title", m_strRank6Title),
                        new SqlParameter("@iRank6Points", m_iRank6Points),
                        new SqlParameter("@strRank7Title", m_strRank7Title),
                        new SqlParameter("@iRank7Points", m_iRank7Points)
                  };

                  //### Add
                  m_iCustomGroupRankID = (int)clsDataAccess.ExecuteScalar("spCustomGroupRanksInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCustomGroupRankID", m_iCustomGroupRankID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@strTag", m_strTag),
                         new SqlParameter("@strRank1Title", m_strRank1Title),
                         new SqlParameter("@iRank1Points", m_iRank1Points),         
                         new SqlParameter("@strRank2Title", m_strRank2Title),
                         new SqlParameter("@iRank2Points", m_iRank2Points),
                         new SqlParameter("@strRank3Title", m_strRank3Title),
                         new SqlParameter("@iRank3Points", m_iRank3Points),
                         new SqlParameter("@strRank4Title", m_strRank4Title),
                         new SqlParameter("@iRank4Points", m_iRank4Points),
                         new SqlParameter("@strRank5Title", m_strRank5Title),
                         new SqlParameter("@iRank5Points", m_iRank5Points),
                         new SqlParameter("@strRank6Title", m_strRank6Title),
                         new SqlParameter("@iRank6Points", m_iRank6Points),
                         new SqlParameter("@strRank7Title", m_strRank7Title),
                         new SqlParameter("@iRank7Points", m_iRank7Points)
                    };
                    //### Update
                    clsDataAccess.Execute("spCustomGroupRanksUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCustomGroupRankID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCustomGroupRankID", iCustomGroupRankID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCustomGroupRanksDelete", sqlParameter);
        }

        public static DataTable GetRanksList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCustomGroupRanksList", EmptySqlParameter);
        }
        public static DataTable GetRanksList(string strFilterExpression, string strSortExpression)
        {
            DataView dvRanksList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvRanksList = clsDataAccess.GetDataView("spCustomGroupRanksList", EmptySqlParameter);
            dvRanksList.RowFilter = strFilterExpression;
            dvRanksList.Sort = strSortExpression;

            return dvRanksList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCustomGroupRankID", m_iCustomGroupRankID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCustomGroupRanksGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
                m_strTag = drRecord["strTag"].ToString();
                m_strRank1Title = drRecord["strRank1Title"].ToString();
                m_iRank1Points = Convert.ToInt32(drRecord["iRank1Points"]);
                m_strRank2Title = drRecord["strRank2Title"].ToString();
                m_iRank2Points = Convert.ToInt32(drRecord["iRank2Points"]);
                m_strRank3Title = drRecord["strRank3Title"].ToString();
                m_iRank3Points = Convert.ToInt32(drRecord["iRank3Points"]);
                m_strRank4Title = drRecord["strRank4Title"].ToString();
                m_iRank4Points = Convert.ToInt32(drRecord["iRank4Points"]);
                m_strRank5Title = drRecord["strRank5Title"].ToString();
                m_iRank5Points = Convert.ToInt32(drRecord["iRank5Points"]);
                m_strRank6Title = drRecord["strRank6Title"].ToString();
                m_iRank6Points = Convert.ToInt32(drRecord["iRank6Points"]);
                m_strRank7Title = drRecord["strRank7Title"].ToString();
                m_iRank7Points = Convert.ToInt32(drRecord["iRank7Points"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}