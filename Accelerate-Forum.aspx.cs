﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Forum : System.Web.UI.Page
{
    private clsAccountUsers clsAccountUsers;
    public clsForums clsForums;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        popForums();
    }

    #region POPULATE METHODS

    protected void popForums()
    {
        int iGroupID = clsAccountUsers.iGroupID;

        DataTable dtGroupList = clsGroups.GetGroupsList("iGroupID=" + iGroupID, "");

        foreach (DataRow dtrGroup in dtGroupList.Rows)
        {
            //int iMasterGroupID = Convert.ToInt32(dtrGroup["iLevelID"]);

            DataTable dtUserForum = clsForums.GetForumsList("iGroupID=" + iGroupID, "strTitle ASC");
            DataTable dtForumComments = new DataTable();

            StringBuilder strbTitles = new StringBuilder();

            string strTitle;
            int iForumID;

            int iCount = 0;

            litForumTopics.Text = "";

            foreach (DataRow dtrAccountMember in dtUserForum.Rows)
            {
                ++iCount;

                iForumID = Convert.ToInt32(dtrAccountMember["iForumID"].ToString());

                clsForums clsCurrentForum = new clsForums(iForumID);
                dtForumComments = clsForumComments.GetForumCommentsList("iForumID=" + iForumID, "");

                strTitle = clsCurrentForum.strTitle;

                strbTitles.AppendLine("<div style='padding-bottom: 20px;'>");
                strbTitles.AppendLine("<b class='media-heading'>" + strTitle + "</b><br />");
                strbTitles.AppendLine(clsCurrentForum.strDescription + "<br />");
                strbTitles.AppendLine("<div style='padding: 4px 0px;'><i>" + dtForumComments.Rows.Count + " Comments</i></div>");
                strbTitles.AppendLine("<a href='Accelerate-Forum-Topic.aspx?iForumID="+iForumID+"' class='anchorAlt'>ENTER FORUM</a><br />");
                strbTitles.AppendLine("</div>");

                //strbTitles.AppendLine("<a href='Accelerate-Forum-Topic.aspx?iForumID=" + iForumID + "' class='linkGreyBlue'>" + strTitle + " <span>created on " + clsCurrentForum.dtAdded.ToString("dd MMM yyyy") + "</span><br><span>" + dtForumComments.Rows.Count + " Comment(s)</span></a><br><br>");
            }

            litForumTopics.Text = strbTitles.ToString();
        }
    }

    #endregion
}