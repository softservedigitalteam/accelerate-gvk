﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accelerate-Welcome-To.aspx.cs" Inherits="Accelerate_Welcome_To" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        @media (max-width: 1920px) {
            .divDescription {
                display: block;
                float: right;
                top: -45%;
            }

            .divLine {
                display: block;
            }

            #btnAdvanceLarge{
                position: relative; 
                display: block; 
                float: right; 
                width: auto; 
                height: auto; 
                max-height: 56px; 
                max-width: 58px; 
                z-index: 99999;
                top: -59%;
                right: -57%;
            }

            #btnAdvanceSmall {
                display:none;
            }

            p{
                font-size: 1.0em;
            }

            
        }

        @media (max-width: 1100px) {

            #btnAdvanceLarge{
                position: relative; 
                display: block; 
                float: right; 
                width: auto; 
                height: auto; 
                max-height: 56px; 
                max-width: 58px; 
                z-index: 99999;
                top: -62%;
                right: -57%;
            }
        }

        @media (max-width: 900px) {

            p{
                font-size: 0.9em;
            }
        }

        @media (max-width: 700px) {
            .divDescription {
                display: block;
                top: 0px;
            }

            .divLine {
                display: block;
            }

            #btnAdvanceSmall{
                position: relative; 
                display: block; 
                float: right; 
                width: auto; 
                height: auto; 
                max-height: 56px; 
                max-width: 58px; 
                z-index: 99999;
            }

            #btnAdvanceLarge {
                display:none;
            }

            @media (max-width: 500px) {
                p{
                    font-size: 0.8em;   
                }
            }
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="max-width: 1920px; max-height: 1080px; width: auto; height: auto; bottom: 0; left: 0; margin: auto; overflow: auto; position: fixed; right: 0; top: 0;">
            <div class="divFinal" style="width: 100%;">
                <asp:Literal runat="server" ID="litImage"></asp:Literal>
                <br />
                <div class="divDescription" style="height: auto; position: relative; background: #9c1f2f;">
                    <div style="width: 80%; padding-top: 20px; padding-bottom: 20px; padding-right: 30px; padding-left: 30px;">
                        <asp:Literal runat="server" ID="litHeading"></asp:Literal><br />
                        <br />
                        <asp:Literal runat="server" ID="litText"></asp:Literal>
                        <%--<p style="font-family: Arial; color: #fff; text-align: left; font-weight: 300;">Congratulations and thank you for making the decision to join Deloitte</p>
                        <p style="font-family: Arial; color: #fff; text-align: left; font-weight: 300;">You join a organisation with thousands of employees.</p>
                        
                        <p style="font-family: Arial; color: #fff; text-align: left; font-weight: 300;">
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                        </p>--%>
                        <br />
                        <asp:LinkButton runat="server" ID="btnSmallProceed" CssClass="btnAdvanceSmall" OnClick="btnSmallProceed_Click"><img id="btnAdvanceSmall"  src="images/btnContinueWelcome.png" /></asp:LinkButton>
                        <%--<a href="Accelerate-Profile-Mission.aspx">
                            <img id="btnAdvanceSmall"  src="images/btnContinueWelcome.png" />
                        </a>--%>
                        <br />

                        <br style="position: relative; clear: both;" />
                    </div>
                    <div id="divLine" style="background: #333333; position: relative; height: 15px;"></div>
                </div>
                 <asp:LinkButton runat="server" ID="btnProceedLarge" CssClass="btnAdvanceSmall" OnClick="btnProceedLarge_Click">
                    <img id="btnAdvanceLarge" src="images/btnContinueWelcome.png" />
                 </asp:LinkButton>

                <div style="position: absolute;">
                </div>
            </div>
        </div>
    </form>
</body>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var iWidth = $(window).width();
        var iHeight = iWidth / 1.777777777777778;
        $('#imgFinal').width(iWidth);
        $('#imgFinal').height(iHeight); 
        $('.divFinal').width(iWidth);
        $('.divFinal').height(iHeight);
        if (iWidth > 700) {
            $('.divDescription').width(iWidth * 0.6607233368532207);
        }
        else {
            $('.divDescription').width(iWidth);
        }
        $('.divLine').width(iWidth);
    });
</script>

<script type="text/javascript">
    $(window).on('resize', function () {
        var iWidth = $(window).width();
        var iHeight = iWidth / 1.777777777777778;
        $('#imgFinal').width(iWidth);
        $('#imgFinal').height(iHeight);
        $('.divFinal').width(iWidth);
        $('.divFinal').height(iHeight);
        if (iWidth > 700) {
            $('.divDescription').width(iWidth * 0.6607233368532207);
        }
        else {
            $('.divDescription').width(iWidth);
        }
        $('.divLine').width(iWidth);
    });
</script>
</html>
