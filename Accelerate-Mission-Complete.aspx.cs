﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Mission_Complete : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsMissions clsMissions;
    int iMissionID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        int iGroupID = clsAccountUsers.iGroupID;
        clsGroups clsGroup = new clsGroups(iGroupID);

        if (clsGroup.iCustomColorID == 0)
        { }
        else
            popStyleSheet(clsGroup.iCustomColorID);

        if(!IsPostBack)
        {
            if ((Request.QueryString["iMissionID"] != "") && (Request.QueryString["iMissionID"] != null))
            {
                iMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);

                //### Populate the form
                popCompletedMessage(iMissionID);
            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        //### Get current mission
        int iMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);

        //saveMissionAndLoadNext(iMissionID);
        //DataTable dtIsVideoMission = clsMissions.GetMissionsList("iMissionID = " + iMissionID, "");
        //foreach (DataRow dr in dtIsVideoMission.Rows)
        //{
        //    if (Convert.ToInt32(dr["iMissionVideoID"]) != 0 && dr["iMissionVideoID"].ToString() != null)
        //    {
        //        Response.Redirect("Accelerate-Video-Missions.aspx");
        //    }
        //}

        Response.Redirect("Accelerate-Home.aspx");
    }

    protected void popCompletedMessage(int iMissionID)
    {
        clsMissions = new clsMissions(iMissionID);
        clsAchievements clsAchievements = new clsAchievements(clsMissions.iAchievementID);

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Achievements\\" + clsAchievements.strPathToImages);

            //### Get the first image from the list
            strHTMLImageProduct = strImagesProduct[0].Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
        }
        catch{}

        StringBuilder strbCurrentMission = new StringBuilder();

        //strbCurrentMission.AppendLine("<span style='text-align:center;color:#3c3c3c;'>You have earned the following badge \"" + clsMissions.strTitle + "\" and " + clsMissions.iBonusPoints + " points.</span><br /><br />");
        strbCurrentMission.AppendLine("<span style='text-align:center;color:#f2f2f2;'>You have earned the following badge \"" + clsMissions.strTitle + "\" and " + clsMissions.iBonusPoints + " points.</span><br /><br />");
        strbCurrentMission.AppendLine("<br /><img src='images/imgStarsUpper.png' /><br /><br /><br />");
        strbCurrentMission.AppendLine("<div class='roundedCornerBox'><img src='" + strHTMLImageProduct + "' style='text-align:center;' width='120px' height='auto'/><br /><br />" + clsMissions.strTitle + "</div><br />");
        strbCurrentMission.AppendLine("<center><div class='buttonStyle' style='padding-top: 5px;'><a href='Accelerate-Home.aspx' style='color: #black;'><div class='deleteButton' style='height:22px; width:100px;'>Continue</div></div></center><br />");
        strbCurrentMission.AppendLine("<br /><img src='images/imgStarsLower.png' /><br /><br /><br />");
        strbCurrentMission.AppendLine("</div>");

        litCurrentMission.Text = strbCurrentMission.ToString();
       // < asp:Button ID = "btnContinue" runat = "server" Text = "Continue" CssClass = "btnStart" Width = "100px" OnClick = "btnContinue_Click" />
    }

    protected void saveMissionAndLoadNext(int iCurrentMissionID)
    {
        string strReportName = "Completed Mission";
        clsMissions clsCurrentMissions = new clsMissions(iCurrentMissionID);
        StringBuilder strbMailBuilder = new StringBuilder();
        //string strMissionTitle ="";
        //int iMissionPoints = 0;
        
        //DataTable dtMissions = clsMissions.GetMissionsList();

        //foreach (DataRow dr in dtMissions.Rows)
        //{
        //    strMissionTitle = dr["strTitle"].ToString();
        //    iMissionPoints = Convert.ToInt32(dr["iBonusPoints"].ToString());
        //    break;
        //}

        //int iMissionCount = dtMissions.Rows.Count;

        bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblNotifications", "iMissionID='" + iCurrentMissionID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

        if (bAlreadyExists)
        {
            clsNotifications clsNotifications = new clsNotifications();

            //### Add / Update
            clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsNotifications.iAddedBy = -10;
            clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsNotifications.iMissionID = iCurrentMissionID;
            clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just completed the mission: " + clsCurrentMissions.strTitle + "";

            clsNotifications.Update();

            clsNotifications clsBadgeNotifications = new clsNotifications();

            //### Add / Update
            clsBadgeNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsBadgeNotifications.iAddedBy = -10;
            clsBadgeNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsBadgeNotifications.iMissionID = iCurrentMissionID;

            clsAchievements clsAchievements = new clsAchievements(clsCurrentMissions.iAchievementID);

            clsBadgeNotifications.strMessage = clsAccountUsers.strFirstName + " has just earned a new badge: " + clsAchievements.strTitle + "";

            clsBadgeNotifications.Update();

            //clsAccountUsers.iCurrentPoints = clsAccountUsers.iCurrentPoints + clsCurrentMissions.iBonusPoints;
            clsAccountUsers.Update();

            Session["clsAccountUsers"] = clsAccountUsers;

            //bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMissions.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

            bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMissions.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID );

            strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
            strbMailBuilder.AppendLine("<table width='100%' bgcolor='#efefef' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
            strbMailBuilder.AppendLine("<tbody>");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
            strbMailBuilder.AppendLine("<tbody>");
            strbMailBuilder.AppendLine("<!-- Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td height='50'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End of Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
            strbMailBuilder.AppendLine("<tbody>");

            strbMailBuilder.AppendLine("<!-- Content -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td width='60'></td>");
            strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, Arial;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
            strbMailBuilder.AppendLine("Dear " + clsAccountUsers.strFirstName + "<br /><br />");
            strbMailBuilder.AppendLine("You have just completed: " + clsAchievements.strTitle + "<br /><br />");
            strbMailBuilder.AppendLine("<br/>");
            strbMailBuilder.AppendLine("Please Login to the system to complete all other missions<br/>");
            strbMailBuilder.AppendLine("" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "/Accelerate-Home.aspx");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("<td width='60'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End of Content -->");

            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("<td width='20'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td height='50'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End of Spacing -->");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("<!-- End of textbanner -->");

            
            clsEmailSettings clsEmailSettings = new clsEmailSettings(true, clsAccountUsers.iAccountUserID);

            if (clsEmailSettings.bEmailMissions)
            {
                if (bUserAchievementExists)
                {
                    clsUserAchievements clsUserAchievements = new clsUserAchievements();
                    clsUserAchievements.iAccountUserID = clsAccountUsers.iAccountUserID;
                    clsUserAchievements.iAchievementID = clsCurrentMissions.iAchievementID;
                    clsUserAchievements.Update();

                    Attachment[] empty = new Attachment[] { };

                    try
                    {
                        missionCompletionEmail.SendMail("no-reply@raonboarding.co.za", clsAccountUsers.strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);

                    }
                    catch { }
                }
            }
        }
    }

    protected void popStyleSheet(int iCustomColourID)
    {
        clsCustomColours clsCustomColours = new clsCustomColours(iCustomColourID);

        litStyleSheet.Text = "<link href='" + clsCustomColours.strLink + "' rel='stylesheet' />";
    }
}
