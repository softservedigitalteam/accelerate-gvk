﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Accelerate_Change_Password : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            {
                //### Redirect back to login
                //Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        if (!IsPostBack)
        {
            //### If the iAccountUserID is passed through then we want to instantiate the object with that iAccountUserID
            if ((Request.QueryString["iAccountUserID"] != "") && (Request.QueryString["iAccountUserID"] != null))
            {
                clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));
                //### Populate the form
            }

        }
        else
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveData();
    }


    #endregion


    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsAccountUsers clCurrentsAccountUsers = new clsAccountUsers(clsAccountUsers.iAccountUserID);
        clCurrentsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clCurrentsAccountUsers.iEditedBy = clsAccountUsers.iAccountUserID;

        //string strHashPassword = clsCommonFunctions.GetMd5Sum("accelerate");
        string strHashPassword = clsCommonFunctions.GetMd5Sum(txtPassword.Text);
        clCurrentsAccountUsers.strPassword = strHashPassword;


        clCurrentsAccountUsers.Update();

        Session["dtAccountUsersList"] = null;

        //### Go back to view page
        Response.Redirect("Accelerate-Home.aspx");
    }

    #endregion


}