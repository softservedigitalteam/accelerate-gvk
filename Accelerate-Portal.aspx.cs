﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;

public partial class Accelerate_Home : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        popPortalDocuments();
    }

    //protected void popPortalFeatures()
    //{
    //    clsPortalFeatures clsPortalFeatures = new clsPortalFeatures();

    //    DataTable dtFeatures = clsPortalFeatures.GetPortalFeaturesList("", "");

    //    StringBuilder strbPortalDoc = new StringBuilder();

    //    int iCount = 0;

    //    dtFeatures.Columns.Add("FullPathForImage");

    //    strbPortalDoc.AppendLine("<div class=''>");

    //    foreach (DataRow dtrFeatures in dtFeatures.Rows)
    //    {
    //        ++iCount;

    //        if ((dtrFeatures["strMasterImage"].ToString() == "") || (dtrFeatures["strMasterImage"] == null))
    //        {
    //            dtrFeatures["FullPathForImage"] = "images/icon.png";
    //        }
    //        else
    //            dtrFeatures["FullPathForImage"] = "PortalFeatures/" + dtrFeatures["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dtrFeatures["strMasterImage"].ToString()) + "" + Path.GetExtension(dtrFeatures["strMasterImage"].ToString());

    //        strbPortalDoc.AppendLine("<div class='span3'>");
    //        strbPortalDoc.AppendLine("<a href='Accelerate-Portal-Feature.aspx?iFeatureID=" + dtrFeatures["iPortalFeatureID"].ToString() + "' class='linkWhite'><div class='holderIcon'><img src='" + dtrFeatures["FullPathForImage"] + "' width='35px' height='auto'/></div><div class='holderText'>");
    //        strbPortalDoc.AppendLine("<h3>" + dtrFeatures["strTitle"].ToString() + "</h3><p>" + dtrFeatures["strDescription"].ToString().Substring(0, 50) + "...</p></div></a>");
    //        strbPortalDoc.AppendLine("</div>");
    //    }

    //    strbPortalDoc.AppendLine("</div>");

    //    litPortalFeatures.Text = strbPortalDoc.ToString();
    //}

    protected void popPortalDocuments()
    {
        clsPortalFeatures clsPortalFeatures = new clsPortalFeatures();

        int iGroupID = clsAccountUsers.iGroupID;

        DataTable dtFeatureGroups = clsGroupFeatures.GetGroupFeaturesList("iGroupID = " + iGroupID, "");

        List<int> lstFeatureIDs = new List<int>();

        foreach (DataRow dr in dtFeatureGroups.Rows)
        {
            lstFeatureIDs.Add(Convert.ToInt32(dr["iPortalFeatureID"]));
        }

        DataTable dtFeatures = clsPortalFeatures.GetPortalFeaturesList("", "");

        StringBuilder strbPortalDoc = new StringBuilder();

        int iCount = 0;
        int iTotalCount = 0;

        dtFeatures.Columns.Add("FullPathForImage");

        foreach (int i in lstFeatureIDs)
        {
            foreach (DataRow dtrFeatures in dtFeatures.Rows)
            {
                if (Convert.ToInt32(dtrFeatures["iPortalFeatureID"]) == i)
                {
                    ++iCount;
                    ++iTotalCount;

                    if ((dtrFeatures["strMasterImage"].ToString() == "") || (dtrFeatures["strMasterImage"] == null))
                    {
                        dtrFeatures["FullPathForImage"] = "images/icon.png";
                    }
                    else
                        dtrFeatures["FullPathForImage"] = "PortalFeatures/" + dtrFeatures["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dtrFeatures["strMasterImage"].ToString()) + "" + Path.GetExtension(dtrFeatures["strMasterImage"].ToString());

                    switch (iCount)
	                {
	                    case 1:
                        strbPortalDoc.AppendLine("<div class='row-fluid'>");
		                strbPortalDoc.AppendLine("<div class='Colour2 KnowledgeBaseBlock'>");
		                break;
	                    case 2:
		                strbPortalDoc.AppendLine("<div class='Colour3 KnowledgeBaseBlock'>");
		                break;
                        case 3:
		                strbPortalDoc.AppendLine("<div class='Colour1 KnowledgeBaseBlock'>");
		                break;
                            case 4:
		                strbPortalDoc.AppendLine("<div class='Colour2 KnowledgeBaseBlock'>");
		                break;
                            case 5:
                        strbPortalDoc.AppendLine("<div class='row-fluid'>");
		                strbPortalDoc.AppendLine("<div class='Colour3 KnowledgeBaseBlock'>");
		                break;
                            case 6:
		                strbPortalDoc.AppendLine("<div class='Colour1 KnowledgeBaseBlock'>");
		                break;
                            case 7:
		                strbPortalDoc.AppendLine("<div class='Colour2 KnowledgeBaseBlock'>");
		                break;
                            case 8:
		                strbPortalDoc.AppendLine("<div class='Colour3 KnowledgeBaseBlock'>");
		                break;
	                }      
                    
                    strbPortalDoc.AppendLine("<b class='media-heading'>"+dtrFeatures["strTitle"]+"</b><br />");

                    string tempstring = dtrFeatures["strDescription"].ToString();

                    if (tempstring.Length < 150)
                        strbPortalDoc.AppendLine(dtrFeatures["strDescription"].ToString() + "<br />");
                    else
                        strbPortalDoc.AppendLine(dtrFeatures["strDescription"].ToString().Substring(0, 150) + "...<br />");

                    strbPortalDoc.AppendLine("<a href='Accelerate-Portal-Feature.aspx?iFeatureID=" + dtrFeatures["iPortalFeatureID"].ToString() + "'>READ MORE</a><br />");
                    strbPortalDoc.AppendLine("</div>"); //Block div end

                    if (iCount % 4 == 0)
                    {
                        if (iCount == 8)
                        {
                            iCount = 0;
                        }

                        strbPortalDoc.AppendLine("</div>"); //row-fluid div end
                        break;
                    }
                    else
                    {
                        if (iTotalCount == dtFeatures.Rows.Count)
                        {
                            strbPortalDoc.AppendLine("</div>"); //info well div end

                        }
                    }
                }             
            }
        }

        strbPortalDoc.AppendLine("<br style='clear:both' />");
        strbPortalDoc.AppendLine("</div>");

        litPortalFeatures.Text = strbPortalDoc.ToString();
    }

    //    protected void popDocuments()
    //    {
    //        clsDocuments clsDocuments = new clsDocuments();

    //        StringBuilder strbDocs = new StringBuilder();

    //        DataTable dtDocuments = new DataTable();

    //        dtDocuments = clsDocuments.GetDocumentsList("", "");

    //        dtDocuments.Columns.Add("FullPathForImage");

    //        //strbDocs.AppendLine("<div class=''>");

    //        foreach (DataRow dtrDocument in dtDocuments.Rows)
    //        {
    //            string strIconUrl = "";
    //            string strDocumentName = "";
    //            string strDocumentPath = ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Documents\\" + dtrDocument["strPathToDocument"].ToString();

    //            strDocumentPath = strDocumentPath.Replace("\\", "/");

    //            string[] files = Directory.GetFiles(strDocumentPath);

    //            if (files.Count() > 0)
    //            {
    //                for (int i = 0; i < files.Count(); i++)
    //                {
    //                    if (files[i].Contains("pdf"))
    //                    {
    //                        strDocumentName = files[i];
    //                    }
    //                }
    //                //strDocumentPath += "+ \"//" + strDocumentName;

    //                strDocumentName = strDocumentName.Replace("/", "\\");
    //                strDocumentPath = strDocumentName.Replace(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", ConfigurationManager.AppSettings["WebRoot"]);
    //                strDocumentPath = strDocumentPath.Replace("Accelerate/", "");
    //            }
    //            strDocumentPath = strDocumentPath.Replace("\\", "/");

    //            if (strDocumentPath.Contains(".pdf"))
    //            {
    //                strIconUrl = "../images/imgPdf.png";
    //            }
    //            else if (strDocumentPath.Contains(".doc") || strDocumentPath.Contains(".docx"))
    //            {
    //                strIconUrl = "../images/imgWord.png";
    //            }

    //            string strShortDescription = "";

    //            if (dtrDocument["strDescription"].ToString().Length > 100)
    //                strShortDescription = dtrDocument["strDescription"].ToString().Substring(0, 100);
    //            else
    //                strShortDescription = dtrDocument["strDescription"].ToString();


    //            strbDocs.AppendLine("<div class='span3'>");
    //            strbDocs.AppendLine("<a href='" + strDocumentPath + "' target='_blank' class='linkWhite'><div class='holderIcon'><img src='" + strIconUrl + "' width='35px' height='auto'/></div><div class='holderText'>");
    //            strbDocs.AppendLine("<h3>" + dtrDocument["strTitle"].ToString() + "</h3><p>" + strShortDescription + "...</p></div></a>");
    //            strbDocs.AppendLine("</div>");

    ////            strbDocs.Append(@"<div class='divDocContainer'>
    ////                                            <div class='divDocInner'>
    ////                                                <a href='" + strDocumentPath + @"' target='_blank'>
    ////                                                    <img src='" + strIconUrl + "' alt='' title='' style='border: none;' />"
    ////                                          + "</a>"
    ////                                          + "</div>"
    ////                                          + "<div class='divDocHeading' style='margin-top: 15px;'><a href='" + strDocumentPath + @"' target='_blank' style='text-decoration: none;' class='DocumentLink'>" + dtrDocument["strTitle"].ToString() + @"</a></div>"
    ////                                          + "<div class='divDocDescription'>" + strShortDescription + @"</div>"
    ////                                          + "<br />"
    ////                                          + "<div class='divDocButton'><a href='" + strDocumentPath + @"' target='_blank' class='viewDetails'>View</a></div>"
    ////                                        + "</div>");
    //        }

    //        if (dtDocuments.Rows.Count > 0)
    //        {
    //            litDocuments.Text = strbDocs.ToString();
    //        }
    //        else
    //        {
    //            litDocuments.Text = "<div style='padding:15px'>There are no documents available for download.</div>";
    //        }
    //    }
}